# WordleAid
 
This application was written in Python and is an aide to use when playing Wordle.

## Installation
To use:
1. download the zip file
1. extract the files
1. run the application (WordleAid.exe) file

## How to Use
Begin by starting the application\
The program will give you a list of options:
```
1) Generate
2) Eliminate
3) Possible
4) Duplicate
5) Eliminate Possible
6) Eliminate Duplicate
7) Insert
8) Reset
9) Quit
```
Type the number listed of the option you want to use and hit *Enter*. For any option that takes input, hit *Enter* to return to the selection menu.
### Generate
`Generate` will display all possible words that match the constraints as well as the word count.
### Eliminate
`Eliminate` will allow you to get rid of letters, **enter all letters at once (no spaces)**, that you know are not possibilities.
### Possible
`Possible` will allow you to choose up to 5 different letters, **enter all letters at once (no spaces)**, that you know are in the word somewhere.
### Duplicate
`Duplicate` will allow you to enter up to 2 different letters, **enter all letters at once (no spaces)**, that occur at least twice in the word.
### Eliminate Possible
`Eliminate Possible` will allow you to specify the locations where you know a possible letter does **not** belong using the format\
    `<letter being removed as a possibility>|<location *1-5* within the word>`
### Eliminate Duplicate
`Eliminate Duplicate` will allow you to specify any letters, that have already been specified as possible, that you know do not appear nore than once.
### Insert
`Insert` will allow you to enter a letter at its confirmed location using the format\
    `<letter confirmed>|<location *1-5* within the word>`
### Reset
`Reset` will allow to wipe the entries already made and start over and will ask for confirmation where you will give either `y` or `n`.
### Quit
`Quit` will simply end and close the application
